<?php 

/*
 * JSON data as raw and need to reformated to match Google Visualization DataTable format
 */
$raw = file_get_contents("http://chart.sandhi.festiware.com/data/data_table.php");

$data = json_decode($raw);

$data_table = array(
  'cols' => array(
    array(
      'label' => 'Update Time',
      'type' => 'string'
    ),
    array(
      'label' => 'VIP',
      'type' => 'number'
    ),
    array(
      'label' => 'Normal',
      'type' => 'number'
    ),
  ),
);

foreach($data as $row)
{
  $temp = array();
  $temp[] = array('v' => (string) $row->sync_time); 
  $temp[] = array('v' => (int) $row->vip); 
  $temp[] = array('v' => (int) $row->normal); 

  $rows[] = array('c' => $temp);
}

$data_table['rows'] = $rows;

$json_table = json_encode($data_table);

echo $json_table;

?>
