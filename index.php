<?php 

$page_title = "WTF Gate Check-in Monitoring";

?>

<?php require "includes/header.php"; ?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?php echo $page_title; ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <hr>
    </div>
  </div>

  <div class="row">
    <div class="col-md-5">
      <div class="block">
        <div id="pie-chart">Pie Chart</div>
      </div>
    </div>
    <div class="col-md-7">
      <div class="block">
        <div id="area-chart">Area Chart</div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="block">
        <div id="table-chart">Table Chart</div>
      </div>
    </div>
  </div>
</div>

<?php require "includes/footer.php"; ?>

