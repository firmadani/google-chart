<script type="text/javascript">

// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['table']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawTableChart);

function drawTableChart() {
  var jsonData = $.ajax({
  url: "get_table_data.php",
  dataType:"json",
  async: false
}).responseText;

// Create our data table out of JSON data loaded from server.
var data = new google.visualization.DataTable(jsonData);

// Instantiate and draw our chart, passing in some options.
var chart = new google.visualization.Table(document.getElementById('table-chart'));
chart.draw(data, {width: '100%', height: '100%'});

}

setInterval(function(){
  // Set a callback to run when the Google Visualization API is loaded.
  drawTableChart();
}, 1000);

</script>
