<script type="text/javascript">

// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawAreaChart);

function drawAreaChart() {

  var jsonData = $.ajax({
    url: "get_data.php",
    dataType:"json",
    async: false
  }).responseText;

  // Create our data table out of JSON data loaded from server.
  var data = new google.visualization.DataTable(jsonData);

  var options = {
    title: '',
    width: '100%', 
    height: '100%',
    backgroundColor: '#e2e2e2'
  };

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.AreaChart(document.getElementById('area-chart'));

  chart.clearChart();
  chart.draw(data, options);
}

setInterval(function(){
  // Set a callback to run when the Google Visualization API is loaded.
  drawAreaChart();
}, 1000);

</script>
