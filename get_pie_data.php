<?php 

/*
 * JSON data as raw and need to reformated to match Google Visualization DataTable format
 */
$raw = file_get_contents("http://chart.sandhi.festiware.com/data/data_pie.php");

$data = json_decode($raw);

$data_table = array(
  'cols' => array(
    array(
      'label' => 'Ticket Name',
      'type' => 'string'
    ),
    array(
      'label' => 'Ticket Total',
      'type' => 'number'
    ),
  ),
);

foreach($data as $row)
{
  $temp = array();
  $temp[] = array('v' => (string) $row->id); 
  $temp[] = array('v' => (int) $row->value); 

  $rows[] = array('c' => $temp);
}

$data_table['rows'] = $rows;

$json_table = json_encode($data_table);

echo $json_table;

?>
